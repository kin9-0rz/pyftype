import os

import pyftype

FIXTURES = os.path.dirname(os.path.abspath(__file__)) + "/fixtures"


def test_guess_extension_file_path():
    ext = pyftype.guess_extension(FIXTURES + "/sample.jpg")
    assert ext is not None
    assert ext == "jpg"


def test_guess_tar():
    ext = pyftype.guess_extension(FIXTURES + "/sample.tar")
    assert ext is not None
    assert ext == "tar"


def test_guess_extension_buffer():
    buf = bytearray([0xFF, 0xD8, 0xFF, 0x00, 0x08])
    ext = pyftype.guess_extension(buf)
    assert ext is not None
    assert ext == "jpg"


def test_guess_extension_buffer_invalid():
    buf = bytearray([0xFF, 0x00, 0x00, 0x00, 0x00])
    ext = pyftype.guess_extension(buf)
    assert ext == "data"


def test_guess_extension_memoryview():
    buf = memoryview(bytearray([0xFF, 0xD8, 0xFF, 0x00, 0x08]))
    ext = pyftype.guess_extension(buf)
    assert ext is not None
    assert ext == "jpg"
