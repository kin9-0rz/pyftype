import os

import pyftype

FIXTURES = os.path.dirname(os.path.abspath(__file__)) + "/fixtures"


def test_guess_mime_file_path():
    mime = pyftype.guess_mime(FIXTURES + "/sample.jpg")
    assert mime is not None
    assert mime == "image/jpeg"


def test_guess_mime_buffer():
    buf = bytearray([0xFF, 0xD8, 0xFF, 0x00, 0x08])
    mime = pyftype.guess_mime(buf)
    assert mime is not None
    assert mime == "image/jpeg"


def test_guess_mime_buffer_invalid():
    buf = bytearray([0xFF, 0x00, 0x00, 0x00, 0x00])
    mime = pyftype.guess_mime(buf)
    assert mime == "application/data"


def test_guess_mime_memoryview():
    buf = memoryview(bytearray([0xFF, 0xD8, 0xFF, 0x00, 0x08]))
    mime = pyftype.guess_mime(buf)
    assert mime is not None
    assert mime == "image/jpeg"
