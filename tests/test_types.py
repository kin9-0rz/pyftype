import os

import pyftype
import pytest

FIXTURES = os.path.dirname(os.path.abspath(__file__)) + '/fixtures'

def test_all_fixtures():
    for root, _, files in os.walk(FIXTURES):
        for f in files:
            path = os.path.join(root, f)
            kind = pyftype.guess(path)
            assert kind.extension in f