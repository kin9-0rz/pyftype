import os

import pyftype

FIXTURES = os.path.dirname(os.path.abspath(__file__)) + "/fixtures"


def test_infer_image_from_disk(benchmark):
    benchmark(pyftype.guess, FIXTURES + "/sample.jpg")


def test_infer_image_from_bytes(benchmark):
    benchmark(pyftype.guess, bytearray([0xFF, 0xD8, 0xFF, 0x00, 0x08]))


def test_infer_video_from_disk(benchmark):
    benchmark(pyftype.guess, FIXTURES + "/sample.mp4")


def test_infer_zip_from_disk(benchmark):
    benchmark(pyftype.guess, FIXTURES + "/sample.zip")


def test_infer_tar_from_disk(benchmark):
    benchmark(pyftype.guess, FIXTURES + "/sample.tar")


def test_infer_video_from_bytes(benchmark):
    benchmark(pyftype.guess, bytearray([0x1A, 0x45, 0xDF, 0xA3, 0x08]))


def test_infer_audio_from_bytes(benchmark):
    benchmark(pyftype.guess, bytearray([0x4D, 0x54, 0x68, 0xA3, 0x64]))


FIXTURES = os.path.dirname(os.path.abspath(__file__)) + '/fixtures'

def test_all_fixtures(benchmark):
    @benchmark
    def do():
        for root, _, files in os.walk(FIXTURES):
            for f in files:
                path = os.path.join(root, f)
                kind = pyftype.guess(path)
                assert kind.extension in f