import os

import pyftype

FIXTURES = os.path.dirname(os.path.abspath(__file__)) + "/fixtures"


def test_guess_file_path():
    kind = pyftype.guess(FIXTURES + "/sample.jpg")
    assert kind is not None
    assert kind.mime == "image/jpeg"
    assert kind.extension == "jpg"


def test_guess_buffer():
    buf = bytearray([0xFF, 0xD8, 0xFF, 0x00, 0x08])
    kind = pyftype.guess(buf)
    assert kind is not None
    assert kind.mime == "image/jpeg"
    assert kind.extension == "jpg"


def test_guess_buffer_invalid():
    buf = bytearray([0xFF, 0x00, 0x00, 0x00, 0x00])
    kind = pyftype.guess(buf)
    assert kind.extension == "data"


def test_guess_memoryview():
    buf = memoryview(bytearray([0xFF, 0xD8, 0xFF, 0x00, 0x08]))
    kind = pyftype.guess(buf)
    assert kind is not None
    assert kind.mime == "image/jpeg"
    assert kind.extension == "jpg"
