# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.2.3](https://gitee.com/kin9-0rz/pyftype/compare/v1.2.2...v1.2.3) (2021-11-04)

### [1.2.2](https://gitee.com/kin9-0rz/pyftype/compare/v1.2.0...v1.2.2) (2021-10-29)

## [1.2.0](https://gitee.com/kin9-0rz/pyftype/compare/v1.1.0...v1.2.0) (2021-10-29)


### Features

* 🎸 支持APK类型检测 ([7c9a04a](https://gitee.com/kin9-0rz/pyftype/commit/7c9a04a6b19bcd25434b5d6ab148f3a63dd24eb3))

## [1.1.0](https://gitee.com/kin9-0rz/pyftype/compare/v1.0.1...v1.1.0) (2021-10-29)


### Features

* 🎸 支持dex、elf等类型10几种类型检测 ([5ae5068](https://gitee.com/kin9-0rz/pyftype/commit/5ae5068d3752789c4ca09f964a67f54e3aa2ce79))

### 1.0.1 (2021-10-28)


### Features

* 🎸 类型检测模块化 ([85570af](https://gitee.com/kin9-0rz/pyftype/commit/85570af4d491a71f407278be10ecaba194f506e2))
