# pyftype

![PyPI](https://img.shields.io/pypi/v/pyftype?style=for-the-badge) ![PyPI - Python Version](https://img.shields.io/pypi/pyversions/pyftype?style=for-the-badge) ![PyPI - License](https://img.shields.io/pypi/l/pyftype?style=for-the-badge) ![PyPI - Status](https://img.shields.io/pypi/status/pyftype?style=for-the-badge) ![PyPI - Downloads](https://img.shields.io/pypi/dw/pyftype?style=for-the-badge)

一个类型检测库。

#### 特性

1. 类型检测完全模块化（直接在modules中编写需要支持的文件类型，无需额外代码）。
2. 主要面向逆向工程方向的类型检测，也支持一般类型检测。

#### 安装

```
❯ pip install pyftype

❯ pyftype -h
usage: pyftype [-h] [-V] p

positional arguments:
  p              path

options:
  -h, --help     show this help message and exit
  -V, --version  show program's version number and exit
                                                                                                                                                                                                                                  
❯ pyftype Makefile
File extension: txt
File MIME type: application/txt
```

#### 用法

```python
import pyftype

# file
f = open(path, "rb")
data = f.read()
kind = pyftype.guess(data)

# path
kind = pyftype.guess(path)

# bytes
buf = bytearray([0xFF, 0xD8, 0xFF, 0x00, 0x08])
kind = pyftype.guess(buf)
```
具体参考examples目录


#### 参考

-  https://github.com/h2non/filetype.py
-  https://github.com/kin9-0rz/cigam
