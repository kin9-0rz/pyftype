import pyftype


def main():
    kind = pyftype.guess("tests/fixtures/sample.apk")
    if kind is None:
        print("Cannot guess file type!")
        return

    print("File extension: %s" % kind.extension)
    print("File MIME type: %s" % kind.mime)


if __name__ == "__main__":
    main()
